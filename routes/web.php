<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group([
    'middleware' => 'auth'
], function() {
    Route::get('/', \App\Http\Livewire\Home\Dashboard::class)->name('home');
    Route::get('/logout', function() {
        Auth::logout();
        return redirect('/login');
    })->name('logout');
    Route::get('/profile', \App\Http\Livewire\User\Profile::class)->name('profile');
});
Route::get('/login', \App\Http\Livewire\Auth\Login::class)->name('login');
Route::get('/register', \App\Http\Livewire\Auth\Register::class)->name('register');
