<?php

namespace Database\Seeders;

use App\Models\Like;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()
            ->count(50)
            ->has(
                Post::factory()->count(3)->has(
                    Like::factory()->count(3)->for(User::factory(), 'owner'), 'likes'
                ),'posts')
            ->create();
    }
}
