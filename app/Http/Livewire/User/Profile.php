<?php

namespace App\Http\Livewire\User;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\TemporaryUploadedFile;
use Livewire\WithFileUploads;

class Profile extends Component
{
    use WithFileUploads;

    public string $successMessage = '';
    public string $name = '';
    public string $currentAvatar = '';
    public TemporaryUploadedFile|string $avatar = '';
    protected array $rules = [
        'name' => ['string','between:3,255','required'],
        'avatar' => [
            'image',
            'mimes:jpeg,png',
            'dimensions:min_width=512,max_width=1024,min_height=512,max_height=1024,ratio=1',
            'max:1024',
        ]
    ];

    public function mount(): void {
        /** @var $user \App\Models\User */
        $user = Auth::user();
        $this->name = $user->name;
        $this->currentAvatar = $user->avatar;
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName);
    }


    public function trim($fieldName): void {
        $this->$fieldName = trim($this->$fieldName);
    }
    public function render()
    {
        return view('livewire.user.profile')
            ->extends('layouts.private')
            ->section('content')
            ->layoutData(['title' => 'Profile']);
    }

    public function save(): void
    {
        $this->validate();
        if ($this->avatar instanceof TemporaryUploadedFile) {
            $filePath = $this->avatar->storeAs('images/users/avatars', $this->avatar->hashName(), ['disk' => 'public']);
        }

        $toBeSaved = Auth::user();

        $toBeSaved->name = $this->name;
        $toBeSaved->avatar = $filePath ?? $this->currentAvatar;
        if ($toBeSaved->save()) {
            $this->successMessage = 'Personal data updated.';
        }
    }
}
