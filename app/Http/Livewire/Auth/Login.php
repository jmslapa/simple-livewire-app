<?php

namespace App\Http\Livewire\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use JetBrains\PhpStorm\NoReturn;
use Livewire\Component;

class Login extends Component
{
    public string $email = '';
    public string $password = '';

    public function render(): View
    {
        return view('livewire.auth.login')->extends('layouts.base')
            ->section('content')
            ->layoutData(['title' => 'Login']);
    }

    #[NoReturn] public function login(): void {
        if (Auth::attempt([
            'email' => $this->email,
            'password' => $this->password
        ])) {
            $this->redirect(route('home'));
        } else {
            $this->addError('password', 'Oops! Please check your credentials.');
        }

    }
}
