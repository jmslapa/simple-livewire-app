<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\TemporaryUploadedFile;
use Livewire\WithFileUploads;

class Register extends Component
{
    use WithFileUploads;

    public string $successMessage = '';
    public string $name = '';
    public string $user_tag = '';
    public string $email = '';
    public string $password = '';
    public string $password_confirmation = '';
    public TemporaryUploadedFile|string $avatar = '';

    protected array $rules = [
        'name' => ['string','between:3,255','required'],
        'user_tag' => ['string','required','max:21','regex:/^@[\w.]+$/', 'unique:users,user_tag'],
        'email' => ['email', 'required', 'unique:users,email'],
        'password' => ['string', 'required', 'min:8', 'max:32', 'required'],
        'password_confirmation' => ['same:password'],
        'avatar' => [
            'image',
            'mimes:jpeg,png',
            'dimensions:min_width=512,max_width=1024,min_height=512,max_height=1024,ratio=1',
            'max:1024',
        ]
    ];

    public function updated($propertyName): void
    {
        if ($propertyName === 'user_tag' && $this->user_tag) {
            $this->user_tag = $this->user_tag[0] === '@' ? $this->user_tag : '@' . $this->user_tag;
        }
        $this->validateOnly($propertyName);
    }

    public function trim($fieldName): void {
        $this->$fieldName = trim($this->$fieldName);
    }

    public function render(): View
    {
        return view('livewire.auth.register')->extends('layouts.base')
            ->section('content')
            ->layoutData(['title' => 'Register']);
    }

    public function register(): void
    {
        $this->validate();
        $filePath = $this->avatar->storeAs('images/users/avatars', $this->avatar->hashName(), ['disk' => 'public']);

        $toBeCreated = new User([
            'name' => $this->name,
            'user_tag' => $this->user_tag,
            'email' => $this->email,
            'password' => bcrypt($this->password),
            'avatar' => $filePath
        ]);

        if ($toBeCreated->save()) {
            $this->successMessage = 'New user registered!';
        }
    }
 }
