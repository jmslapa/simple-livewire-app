<?php

namespace App\Http\Livewire\Components\Nav;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;

class Navbar extends Component
{
    use WithPagination;

    protected string $paginationTheme = 'bootstrap';
    private ?LengthAwarePaginator $paginator = null;
    public string $search = '';

    public function updatingSearch(): void
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.components.nav.navbar', [
            'paginator' => User::query()
                ->when(!$this->search, fn(Builder $query) => $query->whereNull('name'))
                ->when($this->search, fn(Builder $query) => $query->where('name', 'like', "%$this->search%")
                    ->orWhere('user_tag', 'like', "%$this->search%")
                    ->orWhere('email', 'like', "%$this->search%"))
                ->paginate(5)
        ]);
    }
}
