<?php

namespace App\Http\Livewire\Home;

use App\Charts\LikesPerDayChart;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Dashboard extends Component
{
    public string $startDate;
    public string $endDate;

    public function mount(): void {
        $this->startDate = Carbon::create('now')->startOfMonth()->toDateString();
        $this->endDate = Carbon::create('now')->endOfMonth()->toDateString();
    }

    public function render()
    {
        /** @var $user \App\Models\User */
        $user = Auth::user();
        $likesTable = $user->achievedLikes()->getModel()->getTable();
        $postsTable = $user->posts()->getModel()->getTable();
        $period = collect(CarbonPeriod::create($this->startDate, $this->endDate)->toArray())
            ->map(fn(Carbon $p) => $p->toDateString());

        $achievedLikes = $user->achievedLikes()
            ->where("$likesTable.created_at", '>=', $this->startDate)
            ->where("$likesTable.created_at", '<=', $this->endDate)
            ->count();

        $likesPerDay = $user->achievedLikes()->getBaseQuery()
            ->select([
                DB::raw("CAST($likesTable.created_at as DATE) as date"),
                DB::raw("count($likesTable.id) as likesCount")
            ])
            ->where("$likesTable.created_at", '>=', $this->startDate)
            ->where("$likesTable.created_at", '<=', $this->endDate)
            ->groupBy(DB::raw("CAST($likesTable.created_at as Date)"))
            ->get();

        $averageLikesPerDayCount = number_format(
            $likesPerDay->sum('likesCount') / $period->count(),
            2
        );

        $topLikedPosts = $user->posts()
            ->select([
                DB::raw("$postsTable.*"),
                DB::raw("count($likesTable.id) as likesCount")
            ])
            ->join($likesTable, fn(JoinClause $join) => $join
                ->on("$likesTable.post_id", '=', "$postsTable.id"))
            ->where("$likesTable.created_at", '>=', $this->startDate)
            ->where("$likesTable.created_at", '<=', $this->endDate)
            ->groupBy("$postsTable.id")
            ->orderBy('likesCount', 'desc')
            ->limit(5)
            ->get();


        $chart = new LikesPerDayChart();
        $chart->labels($period);
        $chart->dataset(
            "Achieved likes",
            'line',
            $period->map(function ($date) use ($likesPerDay) {
                $item = $likesPerDay->filter(fn($item) => $item->date === $date)->first();
                if ($item) {
                    return $item->likesCount;
                }
                return 0;
            }));

        return view('livewire.home.dashboard', [
            'kpis' => [
                [
                    'title' => 'Achieved likes',
                    'subtitle' => 'Achieved likes in the time range',
                    'value' => $achievedLikes
                ],
                [
                    'title' => 'Average likes per day',
                    'subtitle' => 'Average likes per day in the time range',
                    'value' => $averageLikesPerDayCount
                ]
            ],
            'topLikedPosts' => $topLikedPosts,
            'chart' => $chart
        ])
            ->extends('layouts.private')
            ->section('content')
            ->layoutData(['title' => 'Dashboard']);
    }
}
