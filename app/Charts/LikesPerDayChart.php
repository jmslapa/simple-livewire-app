<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class LikesPerDayChart extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->title('Achieved likes by day of month');
        $this->displayAxes(true);
        $this->height(100);
        $this->options([]);
    }
}
