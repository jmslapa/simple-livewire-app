<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Like extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'post_id'
    ];

    public function post(): BelongsTo {
        return $this->belongsTo(Post::class, 'post_id', 'id');
    }

    public function owner(): BelongsTo {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
