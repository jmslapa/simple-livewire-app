<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'content'
    ];

    public function likes(): HasMany {
        return $this->hasMany(Like::class, 'post_id', 'id');
    }

    public function author(): BelongsTo {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
