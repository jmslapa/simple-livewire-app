<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <link rel="shortcut icon" sizes="114x114" href="{{ asset('laravel-icon.png') }}">
    @vite(['resources/js/app.js', 'resources/sass/app.scss'])
    @livewireStyles

    <style>
        body {
            margin: 0;
            padding: 0;
            width: 100vw;
            height: 100vh;
        }
    </style>
</head>
<body>
    @yield('content')
    @livewireScripts
</body>
</html>
