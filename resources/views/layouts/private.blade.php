<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <link rel="shortcut icon" sizes="114x114" href="{{ asset('laravel-icon.png') }}">
    @vite(['resources/js/app.js', 'resources/sass/app.scss'])
    @livewireStyles

    <style>
        body {
            margin: 0;
            padding: 0;
            width: 100vw;
            height: 100vh;
        }
    </style>
</head>
<body>
    <div class="container w-100 h-100">
        @livewire('components.nav.navbar')
        @yield('content')
        <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
            <div class="col-md-4 d-flex align-items-center">
                <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
                    <svg class="bi" width="30" height="24"><use xlink:href="#bootstrap"></use></svg>
                </a>
                <span class="text-muted">© 2023 João Lapa</span>
            </div>
        </footer>
    </div>
    @livewireScripts
</body>
</html>
