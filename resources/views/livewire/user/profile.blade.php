<div class="my-5">
    <h3>Profile</h3>
    <hr class="my-3"/>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-xl-6">
                @if ($successMessage)
                    <div class="alert alert-success" role="alert">
                        {{ $successMessage }}
                    </div>
                @endif
                <form wire:submit.prevent="save">
                    <div class="mb-3">
                        <label class="form-label" for="avatar">Upload your avatar</label>
                        @if ($avatar || $currentAvatar)
                            <div class="mb-3 d-flex w-100 justify-content-center">
                                <img
                                    src="{{ is_object($avatar) ? $avatar->temporaryUrl() : asset("/storage/$currentAvatar") }}"
                                    width="50%" alt="Avatar preview">
                            </div>
                        @endif
                        <input type="file" class="form-control" id="avatar" wire:model="avatar"/>
                        @error('avatar')
                        <div class="text-danger text-start">{{ $message }}</div> @enderror
                    </div>

                    <div class="mb-3">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" class="form-control" id="name" wire:blur="trim('name')" wire:model="name"
                               placeholder="Type your name">
                        @error('name')
                        <div class="text-danger text-start">{{ $message }}</div> @enderror
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <button type="submit" class="btn btn-primary px-4">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
