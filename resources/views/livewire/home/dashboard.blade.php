<div class="my-5">
    <h2>Dashboard</h2>

    <!-- Filters -->
    <div class="row">
        <div class="h5 mt-3">Filters:</div>
        <div class="col-sm-6">
            <label for="startDate">Start Date</label>
            <input id="startDate" class="form-control" type="date" wire:model="startDate"/>
        </div>
        <div class="col-sm-6 mt-2 mt-sm-0">
            <label for="endDate">End Date</label>
            <input id="endDate" class="form-control" type="date" wire:model="endDate"/>
        </div>
    </div>
    <hr class="mt-4"/>
    <!-- KPIs -->
    <div class="container">
        <div class="row my-5">
            @foreach($kpis as $index => $kpi)
                <div class="col-12 col-md-6 my-2 my-md-0" id="dashboard-kpi-{{ $index }}">
                    <div class="card w-100" style="width: 18rem;">
                        <div class="card-body text-center">
                            <h5 class="card-title">{{ $kpi['title'] }}</h5>
                            <h6 class="card-subtitle mb-2 text-body-secondary">{{ $kpi['subtitle'] }}</h6>
                            <div class="mt-3">
                                <span class="display-1">{{ $kpi['value'] }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <!-- Charts -->
        @if($chart)
            <div style="height: 400px">
                {{ $chart->container() }}
                {{ $chart->script()  }}
            </div>
        @endif

        <!-- Most Liked Table -->
        @if($topLikedPosts->count())
            <div class="d-none d-xl-block mt-5">
                <div class="h3 mt-3 text-center">Most liked posts</div>
                <table class="table table-bordered table-striped">
                    <thead class="table-dark">
                    <tr>
                        <th>Content</th>
                        <th>Date</th>
                        <th>Likes</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($topLikedPosts as $index => $post)
                        <tr id="top-liked-post-{{ $index }}">
                            <td scope="row" class="col-8">
                                {{ $post->content }}
                            </td>
                            <td>{{ $post->created_at }}</td>
                            <td>{{ $post->likesCount }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

    </div>
</div>
