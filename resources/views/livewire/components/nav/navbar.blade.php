<div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="/">Test App</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav w-100">
                <li class="nav-item">
                    <a class="nav-link" href="/">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/profile">Profile</a>
                </li>
                <li class="nav-item ms-lg-auto">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row g-3 mt-2 mb-3">
        <div class="col-lg-12">
            <input class="form-control me-sm-2"
                   wire:model.debounce.500ms="search"
                   type="search"
                   placeholder="Search users"
                   aria-label="Search"
            >
        </div>
    </div>
    <div>
        @if($paginator?->total())
            <div class="h5">Users found:</div>
            <div class="nav-search-result-container">
                @foreach($paginator->items() as $key => $user)
                    <div id="search-result-item-{{$key}}" class="d-flex my-2">
                        <img src="{{ asset("storage/$user->avatar") }}" alt="Avatar" height="48px">
                        <div class="ms-2">
                            <div><strong>{{ $user->user_tag }}</strong> - {{ $user->name }}</div>
                            <div>{{ $user->email }}</div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="d-flex flex-grow-1 justify-content-center align-items-center">
                {{ $paginator->onEachSide(0)->links() }}
            </div>
        @elseif($search)
            <div class="d-flex justify-content-center">
                <div class="h7">No users found...</div>
            </div>
        @endif
    </div>
</div>
