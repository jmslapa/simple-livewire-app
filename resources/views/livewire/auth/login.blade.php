@component('livewire.auth.form-card')
    @slot('title')
        Login
    @endslot
    <div>
        <form wire:submit.prevent="login">
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control" id="email" wire:model="email"
                       placeholder="Type your email">
                @error('email')
                <div class="text-danger text-start">{{ $message }}</div> @enderror
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" class="form-control" id="password" wire:model="password"
                       placeholder="Type your password">
                @error('password')
                <div class="text-danger text-start">{{ $message }}</div> @enderror
            </div>
            <div class="d-flex justify-content-between align-items-center">
                <a href="/register" class="text-decoration-none">I don't have an account.</a>
                <button type="submit" class="btn btn-primary px-4">Send</button>
            </div>
        </form>
    </div>
@endcomponent
