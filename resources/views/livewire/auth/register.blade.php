@component('livewire.auth.form-card')
    @slot('title')
        Register
    @endslot
    <div>
        @if ($successMessage)
            <div class="alert alert-success" role="alert">
                {{ $successMessage }} <a href="/login" class="alert-link">Log in.</a>
            </div>
        @else
            <form wire:submit.prevent="register">
                <div class="mb-3">
                    <label class="form-label" for="avatar">Upload your avatar</label>
                    @if ($avatar)
                        <div class="mb-3 d-flex w-100 justify-content-center">
                            <img src="{{ $avatar->temporaryUrl() }}" width="50%" alt="Avatar preview">
                        </div>
                    @endif
                    <input type="file" class="form-control" id="avatar" wire:model="avatar"/>
                    @error('avatar')
                    <div class="text-danger text-start">{{ $message }}</div> @enderror
                </div>

                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" wire:blur="trim('name')" wire:model="name"
                           placeholder="Type your name">
                    @error('name')
                    <div class="text-danger text-start">{{ $message }}</div> @enderror
                </div>
                <div class="mb-3">
                    <label for="user_tag" class="form-label">User Tag</label>
                    <input type="text" class="form-control" id="user_tag" wire:blur="trim('user_tag')"
                           wire:model="user_tag" placeholder="Type your user tag">
                    @error('user_tag')
                    <div class="text-danger text-start">{{ $message }}</div> @enderror
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" wire:blur="trim('email')" wire:model="email"
                           placeholder="Type your email">
                    @error('email')
                    <div class="text-danger text-start">{{ $message }}</div> @enderror
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" wire:blur="trim('password')"
                           wire:model="password" placeholder="Type your password">
                    @error('password')
                    <div class="text-danger text-start">{{ $message }}</div> @enderror
                </div>
                <div class="mb-3">
                    <label for="password_confirmation" class="form-label">Password Confirmation</label>
                    <input type="password" class="form-control" id="password_confirmation"
                           wire:blur="trim('password_confirmation')" wire:model="password_confirmation"
                           placeholder="Retype your password">
                    @error('password_confirmation')
                    <div class="text-danger text-start">{{ $message }}</div> @enderror
                </div>
                <div class="d-flex justify-content-between align-items-center">
                    <a href="/login" class="text-decoration-none">I already have an account.</a>
                    <button type="submit" class="btn btn-primary px-4">Send</button>
                </div>
            </form>
        @endif
    </div>
@endcomponent
